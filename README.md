# Cleaner Firefox Navbar

A userChrome.css for a cleaner looking navBar in FireFox.

![Screenshot](./screenshot.gif)

How to apply this style? Here's [a tutorial from the firefoxCSS subreddit](https://www.reddit.com/r/FirefoxCSS/wiki/index/tutorials/)

# Support my work

Via [PayPal](https://www.paypal.com/donate/?hosted_button_id=AKHPH2G29QXYU) - Via [liberapay](https://liberapay.com/timkrief/donate) - Via [Ko-Fi](https://ko-fi.com/timkrief)
